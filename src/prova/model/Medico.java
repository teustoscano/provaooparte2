package prova.model;

public class Medico extends Usuario{

	private String especialidade;
	private String registroMed;
	public Consulta consulta;
	
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	public String getRegistroMed() {
		return registroMed;
	}
	public void setRegistroMed(String registroMed) {
		this.registroMed = registroMed;
	}
}
