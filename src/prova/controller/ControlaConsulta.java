package prova.controller;

import java.util.ArrayList;

import prova.model.Consulta;
import prova.model.Medico;
import prova.model.Paciente;

public class ControlaConsulta {
	ArrayList<Consulta> listaConsultas = new ArrayList<Consulta>();
	
	public void adicionarConsulta(Medico medico, Paciente paciente){
		medico.consulta.setMedico(medico);
		medico.consulta.setPaciente(paciente);
		medico.consulta.setOrientacao(null);
		medico.consulta.setDiagnostico(null);
		medico.consulta.setMedicacao(null);
	}
	public void listarConsultas(){
		for(Consulta consulta : listaConsultas){
			System.out.println("Nome Med: "+consulta.getMedico().getNome());
			System.out.println("Nome Med: "+consulta.getPaciente().getNome());
		}
	}
}
