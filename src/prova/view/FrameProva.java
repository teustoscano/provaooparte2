package prova.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;

import prova.controller.ControlaConsulta;
import prova.controller.ControlaMedico;
import prova.controller.ControlaPaciente;
import prova.model.Consulta;
import prova.model.Medico;
import prova.model.Paciente;
import prova.model.Usuario;

public class FrameProva extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textIdade;
	private JTextField textSexo;
	private JTextField textEspecialidade;
	private JTextField textRegistro;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtNomePaciente;
	private JTextField txtNomeP;
	private JTextField txtEM;
	private JTextField txtNomeM;
	private JTextField txtOrientacao;
	private JTextField txtMedicacao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameProva frame = new FrameProva();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameProva() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 814, 454);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final ControlaMedico controlaM = new ControlaMedico();
		final ControlaPaciente controlaP = new ControlaPaciente();
		final ControlaConsulta controlaC = new ControlaConsulta();
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 164, 778, 2);
		contentPane.add(separator);
		
		JLabel foto = new JLabel("New Label");
		foto.setBounds(10, 11, 778, 155);

		ImageIcon imagem = new ImageIcon("provaoo.git/src/prova/img/mais-medicos.png");
		Image logo = imagem.getImage().getScaledInstance(foto.getWidth(), foto.getHeight(), Image.SCALE_DEFAULT);
		
		foto.setIcon(new ImageIcon(logo));
		
		contentPane.add(foto);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 179, 798, 2);
		contentPane.add(separator_1);
		
		//BTN
		JButton btnAcessarMedico = new JButton("Acessar como M\u00E9dico");
		btnAcessarMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//FRAME MEDICO
				final JInternalFrame frameAcessoMedico = new JInternalFrame("New JInternalFrame");
				frameAcessoMedico.setBounds(10, 177, 778, 176);
				contentPane.add(frameAcessoMedico);
				frameAcessoMedico.getContentPane().setLayout(null);
				
				JButton btnListarPacientes = new JButton("Listar Pacientes");
				btnListarPacientes.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for(Paciente paciente : controlaP.listaPaciente){
							JOptionPane.showMessageDialog(null, "Pacientes: "+paciente.getNome());
					}
				}});
				btnListarPacientes.setBounds(357, 69, 125, 66);
				frameAcessoMedico.getContentPane().add(btnListarPacientes);
				
				JButton btnAdicionarPacientes = new JButton("Adicionar Pacientes");
				btnAdicionarPacientes.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Paciente paciente = new Paciente();
						String nome = txtNomePaciente.getText();
						paciente.setNome(nome);
						controlaP.adicionar(paciente);
					}
				});
				btnAdicionarPacientes.setBounds(222, 69, 125, 66);
				frameAcessoMedico.getContentPane().add(btnAdicionarPacientes);
				
				JButton btnNewButton = new JButton("Prestar Consulta");
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//FRAME PRESTAR CONSULTA INTERNO
						final JInternalFrame framePestarConsulta = new JInternalFrame("New JInternalFrame");
						framePestarConsulta.setBounds(0, 177, 788, 170);
						contentPane.add(framePestarConsulta);
						framePestarConsulta.getContentPane().setLayout(null);
						
						JLabel lblNomePaciente_1 = new JLabel("Nome Paciente");
						lblNomePaciente_1.setBounds(10, 11, 76, 14);
						framePestarConsulta.getContentPane().add(lblNomePaciente_1);
						
						txtNomeP = new JTextField();
						txtNomeP.setBounds(96, 8, 224, 20);
						framePestarConsulta.getContentPane().add(txtNomeP);
						txtNomeP.setColumns(10);
						
						JLabel lblNewLabel = new JLabel("Especialidade M\u00E9dica");
						lblNewLabel.setBounds(10, 36, 100, 14);
						framePestarConsulta.getContentPane().add(lblNewLabel);
						
						txtEM = new JTextField();
						txtEM.setBounds(120, 33, 151, 20);
						framePestarConsulta.getContentPane().add(txtEM);
						txtEM.setColumns(10);
						
						JLabel lblNomeMdico = new JLabel("Nome M\u00E9dico");
						lblNomeMdico.setBounds(330, 11, 69, 14);
						framePestarConsulta.getContentPane().add(lblNomeMdico);
						
						txtNomeM = new JTextField();
						txtNomeM.setBounds(409, 8, 346, 20);
						framePestarConsulta.getContentPane().add(txtNomeM);
						txtNomeM.setColumns(10);
						
						JLabel lblOrientaes = new JLabel("Orienta\u00E7\u00F5es");
						lblOrientaes.setBounds(10, 61, 76, 14);
						framePestarConsulta.getContentPane().add(lblOrientaes);
						
						txtOrientacao = new JTextField();
						txtOrientacao.setBounds(96, 58, 224, 71);
						framePestarConsulta.getContentPane().add(txtOrientacao);
						txtOrientacao.setColumns(10);
						
						JLabel lblMedicao = new JLabel("Medica\u00E7\u00E3o");
						lblMedicao.setBounds(330, 61, 69, 14);
						framePestarConsulta.getContentPane().add(lblMedicao);
						
						txtMedicacao = new JTextField();
						txtMedicacao.setBounds(409, 61, 267, 68);
						framePestarConsulta.getContentPane().add(txtMedicacao);
						txtMedicacao.setColumns(10);
						
						JButton btnAdicionarConsulta = new JButton("Adicionar");
						btnAdicionarConsulta.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Medico medico = new Medico();
								Paciente paciente = new Paciente();
								Consulta consulta = new Consulta();
								
								String nome = txtNomeP.getText();
								paciente.setNome(nome);
								String nomeM =txtNomeM.getText();
								medico.setNome(nomeM);
								String esp = txtEM.getText();
								medico.setEspecialidade(esp);
								paciente.setEspecialidadeMedica(esp);
								String orientacao = txtOrientacao.getText();
								consulta.setOrientacao(orientacao);
								String medicamentos = txtMedicacao.getText();
								consulta.setMedicacao(medicamentos);
								
								consulta.setMedico(medico);
								consulta.setPaciente(paciente);
								controlaC.adicionarConsulta(medico, paciente);
							}
						});
						btnAdicionarConsulta.setBounds(686, 36, 76, 44);
						framePestarConsulta.getContentPane().add(btnAdicionarConsulta);
						
						JButton btnVoltarConsulta = new JButton("Voltar");
						btnVoltarConsulta.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								framePestarConsulta.setVisible(false);
							}
						});
						btnVoltarConsulta.setBounds(686, 85, 76, 44);
						framePestarConsulta.getContentPane().add(btnVoltarConsulta);
						framePestarConsulta.setVisible(true);
						
					}
				});
				btnNewButton.setBounds(492, 69, 125, 66);
				frameAcessoMedico.getContentPane().add(btnNewButton);
				
				JLabel lblNomePaciente = new JLabel("Nome Paciente");
				lblNomePaciente.setBounds(10, 11, 90, 14);
				frameAcessoMedico.getContentPane().add(lblNomePaciente);
				
				txtNomePaciente = new JTextField();
				txtNomePaciente.setBounds(110, 8, 165, 20);
				frameAcessoMedico.getContentPane().add(txtNomePaciente);
				txtNomePaciente.setColumns(10);
				
				JButton btnVoltar_2 = new JButton("Voltar");
				btnVoltar_2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frameAcessoMedico.setVisible(false);
					}
				});
				btnVoltar_2.setBounds(627, 69, 125, 66);
				frameAcessoMedico.getContentPane().add(btnVoltar_2);
				frameAcessoMedico.setVisible(true);
				
				
			}
		});
		btnAcessarMedico.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnAcessarMedico.setBounds(10, 358, 319, 40);
		contentPane.add(btnAcessarMedico);
		
		//BTN
		JButton btnAcessarComoPaciente = new JButton("Acessar como Paciente");
		btnAcessarComoPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//INTERNAL FRAME PACIENTE
				final JInternalFrame frameAcessoPaciente = new JInternalFrame("New JInternalFrame");
				frameAcessoPaciente.setBounds(10, 177, 778, 170);
				contentPane.add(frameAcessoPaciente);
				frameAcessoPaciente.getContentPane().setLayout(null);
				
				JLabel lblNome_1 = new JLabel("Nome");
				lblNome_1.setBounds(10, 11, 46, 14);
				frameAcessoPaciente.getContentPane().add(lblNome_1);
				
				txtNome = new JTextField();
				txtNome.setBounds(66, 8, 220, 20);
				frameAcessoPaciente.getContentPane().add(txtNome);
				txtNome.setColumns(10);
				
				JLabel lblCpf = new JLabel("CPF");
				lblCpf.setBounds(301, 11, 46, 14);
				frameAcessoPaciente.getContentPane().add(lblCpf);
				
				txtCpf = new JTextField();
				txtCpf.setBounds(357, 8, 187, 20);
				frameAcessoPaciente.getContentPane().add(txtCpf);
				txtCpf.setColumns(10);
				
				JButton btnAcessarConsulta = new JButton("Acessar Consulta");
				btnAcessarConsulta.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Usuario usuario = new Usuario();
						Paciente paciente = new Paciente();
						String nome = txtNome.getText();
						paciente.setNome(nome);
						String cpf = txtCpf.getText();
						paciente.setCpf(cpf);
						controlaP.acessarConsulta(paciente);
					}
				});
				btnAcessarConsulta.setBounds(512, 39, 115, 90);
				frameAcessoPaciente.getContentPane().add(btnAcessarConsulta);
				
				JButton btnVoltar_1 = new JButton("Voltar");
				btnVoltar_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frameAcessoPaciente.setVisible(false);
					}
				});
				btnVoltar_1.setBounds(637, 39, 115, 90);
				frameAcessoPaciente.getContentPane().add(btnVoltar_1);
				frameAcessoPaciente.setVisible(true);
				
			}
		});
		btnAcessarComoPaciente.setBounds(469, 359, 319, 40);
		contentPane.add(btnAcessarComoPaciente);
		
		//BTN
		JButton btnCadastro = new JButton("Cadastro");
		btnCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final JInternalFrame internalFrame = new JInternalFrame("New JInternalFrame");
				internalFrame.setBounds(121, 177, 637, 182);
				contentPane.add(internalFrame);
				internalFrame.getContentPane().setLayout(null);
				
				JLabel lblNome = new JLabel("Nome");
				lblNome.setBounds(10, 11, 46, 14);
				internalFrame.getContentPane().add(lblNome);
				
				textNome = new JTextField();
				textNome.setBounds(62, 8, 188, 20);
				internalFrame.getContentPane().add(textNome);
				textNome.setColumns(10);
				
				JLabel lblIdade = new JLabel("Idade");
				lblIdade.setBounds(260, 11, 46, 14);
				internalFrame.getContentPane().add(lblIdade);
				
				textIdade = new JTextField();
				textIdade.setBounds(316, 8, 46, 20);
				internalFrame.getContentPane().add(textIdade);
				textIdade.setColumns(10);
				
				JLabel lblSexo = new JLabel("Sexo");
				lblSexo.setBounds(372, 11, 46, 14);
				internalFrame.getContentPane().add(lblSexo);
				
				textSexo = new JTextField();
				textSexo.setBounds(428, 8, 154, 20);
				internalFrame.getContentPane().add(textSexo);
				textSexo.setColumns(10);
				
				JLabel lblEspecialidade = new JLabel("Especialidade");
				lblEspecialidade.setBounds(10, 36, 78, 14);
				internalFrame.getContentPane().add(lblEspecialidade);
				
				textEspecialidade = new JTextField();
				textEspecialidade.setBounds(98, 33, 188, 20);
				internalFrame.getContentPane().add(textEspecialidade);
				textEspecialidade.setColumns(10);
				
				JLabel lblRegistro = new JLabel("Registro");
				lblRegistro.setBounds(296, 36, 66, 14);
				internalFrame.getContentPane().add(lblRegistro);
				
				textRegistro = new JTextField();
				textRegistro.setBounds(346, 36, 171, 20);
				internalFrame.getContentPane().add(textRegistro);
				textRegistro.setColumns(10);
				
				JButton btnCadastrar = new JButton("Cadastrar");
				btnCadastrar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Usuario usuario = new Usuario();
						Medico medico = new Medico();
						String nome = textNome.getText();
						medico.setNome(nome);
						String idade = textIdade.getText();
						medico.setIdade(Integer.parseInt(idade));
						String sexo = textSexo.getText();
						medico.setGenero(sexo);
						String especialidade = textEspecialidade.getText();
						medico.setEspecialidade(especialidade);
						String registro = textRegistro.getText();
						medico.setRegistroMed(registro);
						
						controlaM.adicionar(medico);
						JOptionPane.showMessageDialog(null, "Medico adicionado!");
					}
				});
				btnCadastrar.setBounds(428, 85, 90, 56);
				internalFrame.getContentPane().add(btnCadastrar);
				
				JButton btnVoltar = new JButton("Voltar");
				btnVoltar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						internalFrame.setVisible(false);
					}
				});
				btnVoltar.setBounds(521, 85, 90, 56);
				internalFrame.getContentPane().add(btnVoltar);
				internalFrame.setVisible(true);
			}
		});
		btnCadastro.setBounds(357, 359, 89, 40);
		contentPane.add(btnCadastro);
		
		

		
	}
}
